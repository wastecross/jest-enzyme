# <type>(<scope>): (If applied, this commit will...) <subject> (Max 70 char)
# |<--------------  Using a maximum of 70 characters  -------------->|


# <body> Explain why this change is being made
# |<--------   Try to limit each line to a maximum of 80 characters   -------->|


# <footer> Provide links or keys to any relevant tickets, articles or other resources
# Example: Github issue #23


# --- COMMIT END ---
# The first line cannot be longer than 70 characters, the second line is always
# blank and other lines should be wrapped at 80 characters. The type and scope
# should always be lowercase.
#
# <type> can be
#   feat     New feature for the user, not a new feature for build script
#   fix      Bug fix for the user, not a fix to a build script
#   refactor A code change that neither fixes a bug nor adds a feature
#   style    Formatting, missing semi colons, etc; no production code change
#   docs     Changes to the documentation
#   test     Adding missing tests, refactoring tests; no production code change
#   build    Changes that affect the build system or external dependencies; example: gulp, broccoli, npm
#   ci       Changes to our CI configuration files and scripts; example: Travis, Circle, BrowserStack, SauceLabs
#   perf     A code change that improves performance
#
# The <scope> should be the name of the npm package affected (as perceived by
# the person reading the changelog generated from commit messages. It can be
# empty (e.g. if the change is a global or difficult to assign to a single
# component), in which case the parentheses are omitted.
#
# Revert
# If the commit reverts a previous commit, it should begin with revert: ,
# followed by the header of the reverted commit. In the body it should say:
# This reverts commit <hash>., where the hash is the SHA of the commit being
# reverted.
#
# <subject>
# The <subject> contains a succinct description of the change:
# use the imperative, present tense: "change" not "changed" nor "changes"
# don't capitalize the first letter
# no dot (.) at the end
#
# <body>
# uses the imperative, present tense: “change” not “changed” nor “changes”
# includes motivation for the change and contrasts with previous behavior
#
# <footer>
# The footer should contain any information about Breaking Changes and is also
# the place to reference issues that this commit closes.
#
# Closed issues should be listed on a separate line in the footer prefixed with
# "Closes" keyword like this:
#
# Closes #234
#
# or in the case of multiple issues:
#
# Closes #123, #245, #992
#
# All breaking changes have to be mentioned in footer with the description of
# the change, justification and migration notes. They should start with the word
# BREAKING CHANGE: with a space or two newlines. The rest of the commit message
# is then used for this.
#
# example:
#   BREAKING CHANGE:
#
#   `port-runner` command line option has changed to `runner-port`, so that it is
#   consistent with the configuration file syntax.
#
#   To migrate your project, change all the commands, where you use `--port-runner`
#   to `--runner-port`.
#
# --------------------
# Remember to
#    Do not capitalize the subject line
#    Use the imperative mood in the subject line
#    Do not end the subject line with a period
#    Separate subject from body with a blank line
#    Use the body to explain what and why vs. how
#    Can use multiple lines with "-" for bullet points in body
#
# ------ HOW TO USE THIS TEMPLATE ------
# git config commit.template .git-commit-template
# --------------------