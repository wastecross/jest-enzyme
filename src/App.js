import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css';
import home from './assets/home.png';
import Counter from './components/Counter/Counter';
import Jotto from './components/Jotto/Jotto';

class App extends Component {
  home = () => {
    this.props.history.push('/');
  };

  jotto = () => {
    this.props.history.push('/jotto');
  };

  counter = () => {
    this.props.history.push('/counter');
  };

  render() {
    return (
      <div data-test='component-app' className="App">
        <div className="App-header">
          <div className="App-logo">
            <button onClick={this.home}>
              <img src={home} alt='home' width='48px' height='48px' />
            </button>
          </div>
          <button onClick={this.counter}>Counter</button>
          <button onClick={this.jotto}>Jotto</button>
        </div>
        <div className="App-Container">
          <Route path="/jotto" component={Jotto} />
          <Route path="/counter" component={Counter} />
        </div>
      </div>
    );
  }
}

export default () => (
  <div>
    <Router data-test='router'>
      <Route data-test='component-app' component={App} />
    </Router>
  </div>
);
