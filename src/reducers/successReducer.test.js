import { actionTypes } from '../actions/index';
import succesReducer from './successReducer';

test('returns default initial state of false when no action', () => {
  const newState = succesReducer(undefined, {});
  expect(newState).toBe(false);
});

test('retuns state of true upon receiving an action of type correct', () => {
  const newState = succesReducer(undefined, {
    type: actionTypes.CORRECT_GUESS,
  });
  expect(newState).toBe(true);
});
