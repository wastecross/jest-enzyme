import React, { Component } from 'react';
import { connect } from 'react-redux';
import GuessedWords from './GuessedWords/GuessedWords';
import Congrats from './Congrats/Congrats';
import Input from './Input/Input';
import { getSecretWord } from '../../actions/index';
import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css';

export class UnconnectedJotto extends Component {
  /**
   * @method componentDidMount
   * @returns {undefined}
   */
  componentDidMount() {
    // get the secret word
    this.props.getSecretWord();
  }

  render() {
    return (
      <div data-test='component-jotto' className='container'>
        <span>The secret word is {this.props.secretWord}</span>
        <Congrats success={this.props.success} />
        <Input />
        <GuessedWords guessedWords={this.props.guessedWords} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { success, guessedWords, secretWord } = state;
  return { success, guessedWords, secretWord };
};

export default connect(mapStateToProps, { getSecretWord })(UnconnectedJotto);
