import React, { Component } from "react";

class Counter extends Component {
  constructor(props) {
    super(props);

    this.state = {
      counter: 0,
      isError: false,
    };
  }

  render() {
    return (
      <div data-test="component-counter">
        <h1 data-test="counter-display">
          The counter is currently {this.state.counter}
        </h1>
        <p data-test="message-error">
          {this.state.isError ? "No count below 0" : ""}
        </p>
        <button
          onClick={() =>
            this.setState({ counter: this.state.counter + 1, isError: false })
          }
          data-test="increment-button"
        >
          Increment counter
        </button>
        <button
          onClick={() => {
            if (this.state.counter === 0) {
              this.setState({ isError: true });
            } else {
              this.setState({ counter: this.state.counter - 1 });
            }
          }}
          data-test="decrement-button"
        >
          Decrement counter
        </button>
      </div>
    );
  }
}

export default Counter;
