import React from 'react';
import { shallow } from 'enzyme';
import { findByTestAttr } from '../../../test/testUtils';
import Counter from './Counter';

/**
 * Factory function to create a shallowWrapper for the Counter component
 * @function setup
 * @param {object} props - Component props specific to this setup.
 * @param {object} state - Initial state for setup.
 * @returns {ShallowWrapper}
 */
const setup = (props = {}, state = null) => {
  const wrapper = shallow(<Counter {...props} />);
  if (state) wrapper.setState(state);
  return wrapper;
};

test('renders without crashing', () => {
  const wrapper = setup();
  const counterComponent = findByTestAttr(wrapper, 'component-counter');
  expect(counterComponent.length).toBe(1);
});

test('renders increment button', () => {
  const wrapper = setup();
  const button = findByTestAttr(wrapper, 'increment-button');
  expect(button.length).toBe(1);
});

test('renders counter display', () => {
  const wrapper = setup();
  const counterDisplay = findByTestAttr(wrapper, 'counter-display');
  expect(counterDisplay.length).toBe(1);
});

test('counter starts at 0', () => {
  const wrapper = setup();
  const initialCounterState = wrapper.state('counter');
  expect(initialCounterState).toBe(0);
});

test('clicking button increments counter', () => {
  const counter = 7;
  const wrapper = setup(null, { counter });
  const button = findByTestAttr(wrapper, 'increment-button');
  button.simulate('click');
  const counterDisplay = findByTestAttr(wrapper, 'counter-display');
  expect(counterDisplay.text()).toContain(counter + 1);
});

test('clicking button decrements counter', () => {
  const counter = 13;
  const error = false;
  const wrapper = setup(null, { counter, error });
  const button = findByTestAttr(wrapper, 'decrement-button');
  button.simulate('click');
  const counterDisplay = findByTestAttr(wrapper, 'counter-display');
  expect(counterDisplay.text()).toContain(counter - 1);
});

test('show error when decrement button is clicked and count is 0', () => {
  const counter = 0;
  const error = false;
  const wrapper = setup(null, { counter, error });
  const button = findByTestAttr(wrapper, 'decrement-button');
  button.simulate('click');
  const messageError = findByTestAttr(wrapper, 'message-error');
  expect(messageError.text()).toContain('No count below 0');
});

test('hide error when increment button is clicked', () => {
  const counter = 0;
  const error = true;
  const wrapper = setup(null, { counter, error });
  const button = findByTestAttr(wrapper, 'increment-button');
  button.simulate('click');
  const messageError = findByTestAttr(wrapper, 'message-error');
  expect(messageError.text()).toContain('');
});
